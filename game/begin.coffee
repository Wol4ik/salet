salet.game_id = "your-game-id-here"
salet.game_version = "1.6"
$(document).ready(() ->
  window.addEventListener('popstate', (event) ->
    salet.goBack()
  )
  $("#night").on("click", () ->
    if (window.night)
      styles = {
        "-webkit-filter": ""
        "filter": ""
        "background-color": ""
      }
      $("body").css(styles)
      $("#night").removeClass("active")
      window.night = false
    else
      styles = {
        "-webkit-filter": "invert(1)hue-rotate(180deg)"
        "filter": "invert(1)hue-rotate(180deg)"
        "background-color": "#000"
      }
      $("body").css(styles)
      $("#night").addClass("active")
      window.night = true
  )

  salet.beginGame()
)

###
Element helpers. There is no real need to build monsters like a().id("hello")
because you won't use them as is. It does not make sense in context, the
author has Markdown and all utilities to *forget* about the markup.
###
way_to = (content, ref) ->
  return "<a href='#{ref}' class='way'>#{content}</a>"
textlink = (content, ref) ->
  return "<a href='./_writer_#{ref}' class='once'>#{content}</a>"
actlink = (content, ref) ->
  return "<a href='./#{ref}' class='once'>#{content}</a>"

# The first room of the game.
# For accessibility reasons the text is provided in HTML, not here.
room "start",
  enter: () ->
    salet.character.bought_lamp = false
  dsc: """
  """,
  choices: "#start"

# This is a special inventory room.
# The inventory button is a regular link to this room.
# You may alter these as much as you like or scrap it along with the button.
room "inventory",
  canSave: false # saving the game here is forbidden. Aautosaving too.
  enter: () ->
    $("#inventory").hide()
  exit: () ->
    $("#inventory").show()
  dsc: () ->
    if salet.character.inventory.length == 0
      text = "You are carrying nothing."
    else
      text = "You are carrying:\n\n"
      for thing in salet.character.inventory
        text += "* #{salet.character.listinv(thing.name)}\n"
    return text+"\n\n"+"""
    <div class="center"><a href="./exit"><button class="btn btn-lg btn-outline-primary">Go back</button></a></div>
    """
  actions:
    exit: () ->
      return salet.goBack()

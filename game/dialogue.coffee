###
A dialogue shortcut.
Usage:

    dialogue "Point out a thing in her purse (mildly)", "start", "mild", """
        Point out a thing in her purse (mildly)
      """, "character.mild = true"
###
dialogue = (title, startTag, endTag, text, effect) ->
  retval = room("dialogue_"+Object.keys(salet.rooms).length, {
    optionText: title
    dsc: text
    clear: false # backlog is useful in dialogues
    choices: "#"+endTag
  })
  if typeof(startTag) == "string"
    retval.tags = [startTag]
  else if typeof(startTag) == "object"
    retval.tags = startTag
  if effect?
    retval.before = (character, system) ->
      eval(effect)
  return retval
